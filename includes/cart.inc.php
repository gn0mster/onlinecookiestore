<?php # cart.inc.php 
    require_once ('db.inc.php'); //Connect to the database
	$action = $_GET['action']; //the action from the URL 
    $total_cost_of_items = 0;
	
	switch($action) {	//decide what to do	
	
	case 'add':
		$product_id = $_GET['id'];	 //the product id from the URL 
			if(!isset($_SESSION['cart'][$product_id])){
				$_SESSION['cart'][$product_id]= 0;
			}
			$_SESSION['cart'][$product_id]++;
			echo '<h2>Item Added!</h2>';
            echo 'Successfully added product to your cart!<br /><br />';
            echo '<a href="index.php?page=cart&action=view">View Cart</a>';
			echo '<br />';
		    echo '<br />';
		    echo '<a href="index.php?">Continue Shopping</a>';	
	        break;
			
	  case 'view':
		if($_SESSION['cart']) { 
			echo '<h2>Cart Contents:</h2>';
			echo '<table border=0 width=100% cellpadding=0 style="font-size:16px">'; 
			echo '<tr align="left">';
			echo '<th>Name of Selected Cookies</th>';
			echo '<th>Price per dozen</th>';
			echo '<th align="center">Quantity</th>';
			echo '<th align="center">Remove</th>';
			
			foreach($_SESSION['cart'] as $product_id => $quantity) {
				$q = sprintf("SELECT name, price_per_dozen FROM cookies WHERE cookie_id = '$product_id'");
				$r = @mysqli_query ($dbc, $q); //Run this query
		        if( !$r )
                echo mysqli_error($dbc);		
				
	        	if(mysqli_num_rows($r) > 0) {
            		list($name, $price_per_dozen) = mysqli_fetch_array($r, MYSQL_BOTH);
          			$line_cost = $price_per_dozen * $quantity;
           	    	$total_cost_of_items = number_format($total_cost_of_items + $line_cost, 2);
					echo '<tr>';
                	echo '<td>' . $name . '</td>';
                	echo '<td>$' . $price_per_dozen . '</td>';
                	echo '<td align="center">' . $quantity . '</td>';
			    	echo '<td align="center"><a href="index.php?page=cart&action=remove&id='. $product_id .'">X</a></td>';
					echo '</tr>';
					
				}				
			}
			        echo '</table><br />';
			        echo 'Total:  $' . $total_cost_of_items . '<br /><br />';
			        echo "<td><a href=\"index.php?page=cart&action=empty\" onclick=\"return confirm('Remove all cookies from your cart? Are you sure?');\">Empty Cart</a></td>";
		} else {
			 echo 'You have no cookies in your cart!';
		} 
	         break;
		
    case 'remove':
		$product_id = $_GET['id'];	 //the product id from the URL 
			if(!isset($_SESSION['cart'][$product_id])){
				$_SESSION['cart'][$product_id]= 0;
			}
			$_SESSION['cart'][$product_id]--; //remove one from the quantity of the product with id $product_id 
			if($_SESSION['cart'][$product_id] == 0) unset($_SESSION['cart'][$product_id]); //if the quantity is zero, remove it                completely (using the 'unset' function) - otherwise is will show zero, then -1, -2 etc when the user keeps removing                items. 
		        echo '<h3>Item Removed!</h3>';
                echo 'Your item was removed from your cart!<br /><br />';
                echo '<a href="index.php?page=cart&action=view">View Cart</a>';	
		echo '<br />';
		echo '<br />';
		echo '<a href="index.php?">Continue Shopping</a>';	
	    break;
		 
		case 'empty':
			unset($_SESSION['cart']); //unset the whole cart, i.e. empty the cart. 
                     echo '<h3>Cart is empty!</h3>';
                     echo 'You have removed all the cookies from your cart!<br /><br />';
		             echo '<br />';
		             echo '<br />';
		             break; 
		 
	 }

?>

