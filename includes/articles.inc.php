<?php 
     date_default_timezone_set('America/Chicago'); 
	//include_once('db.inc.php');
	
		// make the query:
		$q = "SELECT * FROM articles, system_users";
		$r = @mysqli_query ($dbc, $q); // run request.	
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		if (!$r) { // print results:
			$error = 'Error fetching articles';
			exit();
	} else {
		if(isset($_GET['page']) && !empty($_GET['id'])) {
		echo '<h2>' . $row['article_title'] . '</h2>';
		echo '<p class="thick">'  . $row['article_title'] . '</strong></p>';
		echo '<p>Published ' .date("D, M j, Y", strtotime($row['publication_date'])) . ' by ' . ucfirst($row['username']) . '</p>';
		echo '<p>' . $row['article_content'] . '</p>';
		
	} else {
		echo '<h3>Chunky Articles</h3>';
		echo '<p><a href="index.php?page=articles&id=' . $row['article_id'] . '">'  . $row['article_title'] . '</a></p>';
		echo '<p>Published ' .date("D, M j, Y", strtotime($row['publication_date'])) . ' by ' . ucfirst($row['username']) . '</p>';
		echo '<p>' . $row['article_synopsis'] . '</p>';
	}
		}
?>