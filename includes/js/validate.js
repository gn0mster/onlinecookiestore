//validate.js

$(document).ready(function() {
	$('.errorMessage').hide();
	$('.newserrorMessage').hide();
           debug: true,
		  
    $("#contact_form").submit(function() {

		var email_from = document.getElementById("email_from");
		var email_fromval = email_from.value; 
		
		
     if (isEmail(email_fromval)) {
		  $('#email_from') .removeClass ('error');
		  $('#email_error').hide();
		  
		  $('.content-holdera').hide();
		  $('#submit').hide();
		  $('#emailmessageSuccess').fadeIn("slow");
		  
	  } else {
		  
		  $('#email_from') .addClass ('error');
		  $('#submit').show();
		  $('#email_error').fadeIn();
		
	  }
	  
	  var message = document.getElementById("message");
	  var min_char = message.value.length;

	  if (min_char > 4) {
		  message = $('#message').val();
		  $('#message') .removeClass ('error');
		  $('#message_error').hide();
		  
		  $('.content-holderb').hide();
		  $('#submit').hide();
		  $('#messageSuccess').fadeIn("slow");
		  
	  } else {
		  
		  $('#message') .addClass ('error');
		  $('#submit').show();
		  $('#message_error').fadeIn();
	
	  }
	  
	   
	return false;
		
	  
	});
	
	$('#newsletter_form').submit(function() {
            
		var email_address = document.getElementById("email_address");
		var email_addressnews = email_address.value;
	
		if (isEmail(email_addressnews)) {
								
			$('#newsletterError').removeClass('error');
			$('#newsletterError').hide();
			
			$('.contenta').hide();
			$('#newsletterSuccess').fadeIn("slow");
		
		} else { 
			
	    	$('#newsletterError').addClass('error');
			$('#newsletterError').fadeIn();
		}
	
	   return false;
	   
    });
	
    $('#register_form').submit(function() {
		
        var val = $('#email_register').val();
        var error = false;
		
        if(isEmail(val)) {
         $('#registerError').hide();
		 
       } else { // get error
	   
           error = true;
           $('#registerError').fadeIn();
       }
	   
       var p1 = $('#password_one').val();
       var p2 = $('#password_two').val();
 
       if(p1.length == 0) { // get error
	   
	       error = true;
		   $('#p1Error').fadeIn();
		   
	   } else {
		   
		   $('#p1Error').hide();
	   }
	   
	   if(p2.length == 0 || p1 != p2) { // get error
		   
		   error = true;
		   $('#p2Error').fadeIn();
		   
	   } else {
		   
		   $('#p2Error').hide();
	   }

       if (error) {
            return false;
	   } else {
		    return true;

	   }
   }); // end of form submission

});


function isEmail(email) {

  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

