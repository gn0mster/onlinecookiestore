<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
        <script src="includes/js/validate.js"  type="text/javascript"></script>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" lang="en" />
        <link href="includes/css/styles.css" rel="stylesheet" type="text/css" />
	<title>Chunky Charlies Cookies, Inc - Home</title>
</head>
<body>
   <div id="wrapper">
	<div id="header"><img src="includes/images/logo.png" alt="logo image" /><img src="includes/images/cookiebasket2.png" class="basket" alt="logo image" />
			<!--
				This will contain your header info
			-->
	   <div id="account-information">
			<?php include('includes/account-information.inc.php'); ?>
        </div>
        </div>
		<div id="navigation-one">
			<ul class="claybricks">
				<li><a href="index.php?">Home</a></li>
				<li><a href="index.php?page=about">About Us</a></li>
				<li><a href="index.php?page=articles">Chunky Articles</a></li>
				<li><a href="index.php?page=contact">Contact Us</a></li>
			</ul>
		</div>
	<div id="main">
           <div id="cookie-categories">
                 <?php 
		       include ('includes/categories.inc.php');
	          ?>		  
                 
	  </div>
			<div id="shopping-cart">
				<h4>Your Shopping Cart</h4>
			  <div class="content">
			 <?php 
		       include ('includes/cart_items.inc.php');
	          ?>		  
			  </div>
			</div>
		  <div id="content-holder">
                      <?php
                       $page = '';
				  if(isset($_GET['page'])&&(!empty($_GET['page']))){
					  $page = $_GET['page'];
					  switch(strtolower($page)){
						case 'about':
					      include 'includes/about.inc.html';
						  break;
						case 'articles':
						  include('includes/articles.inc.php');
						  break;
						case 'contact':
						  include 'includes/contact.inc.php';
						  echo'<div id="emailmessageSuccess">';
				          echo'<img src="includes/images/emailty.png" alt="email success image" />';
                          echo'</div>';
                          echo'<div id="messageSuccess">';
				          echo'<img src="includes/images/msgsuccess.png" alt="msg success image" />';
                          echo'</div>';
						  break; 
						case 'register':
						  include'includes/register.inc.php';
						  break;     
						case 'sign-in':
						  include('includes/sign-in.inc.php');
						  break;  
						case 'log-off':
						  include('includes/log-off.inc.php');
						  break; 
						case 'admin':
						  include('includes/profile.inc.php');
						  break;       
						case 'view':
						  include('includes/product.inc.php');
						  break;  
						case 'cart':
						  include('includes/cart.inc.php');
						  break;      
					   //...the logic for other pages...
					    default:
						  echo '<h1>No Page Found!</h1>';
						  //The requested page does not exist	  
                  }
                } else {
					//display the homepage
					include 'includes/products.inc.php';
				}
              ?>
              </div>
              <div id="newsletter">
                <?php
				include('includes/newsletter.inc.php');
				?>
	       </div>
           <div id="newsletterSuccess">
				<img src="includes/images/success.png" alt="success image" />
            </div>
           </div>	    
	      <div id="footer">
	         &copy;  2012 Chunky Charlies Cookies, Inc. - All Rights Reserved <a href="index.php?page=contact">Contact US</a>
             </div>		
   </div>
</body>
</html>