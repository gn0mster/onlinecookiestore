-- MySQL dump 10.11
--
-- Host: localhost    Database: ngsee_12SS_2750
-- ------------------------------------------------------
-- Server version	5.1.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_title` varchar(60) NOT NULL,
  `article_synopsis` varchar(255) NOT NULL,
  `article_content` text NOT NULL,
  `publication_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL,
  `publisher_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`publisher_id`),
  KEY `fk_articles_system_users1` (`publisher_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (5,'Historically Speaking...','Part 1 of a 2-part series on the history of Chunky Charlie\'s Cookies.','Charles Lynn Dupre was born in Omaha, Nebraska on a cold winter\'s day in January of 1965.  Charles was a hungry child with a taste for sweets.  He loved chocolate, butter, creme, and anything filled with saturated fats and processed sugars.  Little did young Charlie know that someday, his love of all things bad would pay dividends.','2012-05-01 20:02:30','2012-05-01 20:02:30',0);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Baked Cookies'),(2,'Cookie Doughs'),(5,'Evil Cookies'),(4,'Fat Free Cookies'),(3,'Girl Scout Cookies'),(6,'Chunky Cookies');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cookies`
--

DROP TABLE IF EXISTS `cookies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cookies` (
  `cookie_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `short_description` varchar(50) NOT NULL,
  `long_description` varchar(512) NOT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `price_per_dozen` float(6,2) NOT NULL,
  `seasonal_favorite` tinyint(1) NOT NULL,
  PRIMARY KEY (`cookie_id`,`category_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_cookies_categories` (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cookies`
--

LOCK TABLES `cookies` WRITE;
/*!40000 ALTER TABLE `cookies` DISABLE KEYS */;
INSERT INTO `cookies` VALUES (5,1,'Chunky Charlie\'s Chewy Chocotastic Chunker','The cookie that started it all!','The cookie that started it all!  The Chocotastic Chunker remains our best seller.  16oz of gooey, chocolatey, buttery flavor in every bite!','sample_cookie_1_120x120.png',12.00,0),(6,2,'Chunky Charlie\'s Heart-Healthy Hunk-o-Chocolate','A healthy start, to a healthy dessert!','The Heart-Healthy Hunk-o-Chocolate Cookie Dough is a reduced-fat, enhanced-flavored cookie dough full of chemicals, flavor and fun, but short on the calories!','sample_cookie_1_120x120.png',15.00,0),(9,4,'Uncle Vernon\'s Funk-O-Ramic Chocotastic Chew!','Uncle Vernon\'s favorite is back and with a vengean','Uncle Vernon love funky chocolate.  Each Funk-o-Ramic Chew consists of 24oz of pure chocofunky fun!','sample_cookie_1_120x120.png',12.00,0),(10,5,'Gramma\'s Great Gooey Gums!','Chewy, for the weak of tooth!','Gramdmama may have no teeth, but she still loves her sweets!  Give gramdmama the softest, chewiest cookie in existence!  18oz of butter and chocolate with a little butter makes this cookie to die for!','sample_cookie_2_120x120.png',12.00,0);
/*!40000 ALTER TABLE `cookies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `pass` char(40) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(2) NOT NULL,
  `zip` varchar(10) NOT NULL,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acknowledged` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (11,'see.naomi@gmail.com','hello','2012-07-08 04:30:37',0),(12,'naomi.see@seenaomi.net','Eureka! at 1:40am!','2012-07-08 06:49:24',0),(13,'see.naomi@gmail.com','you are not working on the server!','2012-07-08 06:54:48',0),(14,'naomi.see@seenaomi.net','working on the server would be nice!','2012-07-08 06:58:38',0),(15,'naomi.see@seenaomi.net','please just be delayed ug...','2012-07-08 07:03:20',0),(16,'naomi.see@seenaomi.net','why are you not working?','2012-07-08 15:54:06',0),(17,'1@msn.com','s','2012-07-08 17:18:20',0),(18,'42@msn.com','Life, the universe, and everything.','2012-07-08 17:19:28',0),(19,'see.naomi@gmail.com','hrm','2012-07-08 23:08:31',0),(20,'see.naomi@gmail.com','broken.','2012-07-08 23:08:51',0),(21,'monkey@doo.net','broken.','2012-07-09 03:49:26',0),(22,'see.naomi.edu','dorkus','2012-07-10 01:13:11',0),(23,'see.naomi.edu','dorkus','2012-07-10 01:20:55',0),(24,'see.naomi.edu','dorkus','2012-07-10 01:34:55',0),(25,'chunkycheryl@cookie.com','Hey I sure do love your cookies!','2012-07-12 02:16:18',0),(26,'chunkycheryl@cookie.com','Hey I sure do love your cookies!','2012-07-12 02:23:03',0),(27,'toolegit@tooquit.com','hooray','2012-07-12 02:23:40',0),(28,'toolegit@tooquit.com','hooray','2012-07-12 02:39:53',0),(29,'1@msn.com','Test message, yo!','2012-07-15 04:06:52',0),(30,'see.naomi@gmail.com','12354','2012-08-05 16:54:25',0);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `requestor_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '		',
  `requestor_email` varchar(60) NOT NULL,
  PRIMARY KEY (`requestor_id`),
  UNIQUE KEY `requestor_email_UNIQUE` (`requestor_email`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
INSERT INTO `newsletter` VALUES (1,'naomi.see@seenaomi.net'),(2,'see.naomi@science.com'),(3,'see.naomi@gmail.com'),(4,'dork@bro.net'),(5,'testguy@email.com'),(6,'testguy2@email.com'),(7,'test'),(8,'naomi.see@gmail.com'),(9,'sgasgasg@safa.com'),(10,'ngsee@drititle.com');
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_content`
--

DROP TABLE IF EXISTS `order_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_content` (
  `oc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL,
  `price` float(6,2) unsigned NOT NULL,
  `ship_date` datetime NOT NULL,
  PRIMARY KEY (`oc_id`,`cookie_id`,`order_id`),
  KEY `fk_order_content_cookies1` (`cookie_id`),
  KEY `fk_order_content_orders1` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_content`
--

LOCK TABLES `order_content` WRITE;
/*!40000 ALTER TABLE `order_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `total` float(10,2) unsigned NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`,`customer_id`),
  KEY `fk_orders_customers1` (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_users`
--

DROP TABLE IF EXISTS `system_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` char(40) NOT NULL,
  `street_address` varchar(50) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip_code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_users`
--

LOCK TABLES `system_users` WRITE;
/*!40000 ALTER TABLE `system_users` DISABLE KEYS */;
INSERT INTO `system_users` VALUES (0,'administrator','',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `system_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-13 20:33:48
